import Vue from 'vue'
import App from './App.vue'
import VueMultipleImageClipping from './components/VueMultipleImageClipping'
import VuejsClipper from 'vuejs-clipper'
import VueRx from 'vue-rx'

Vue.config.productionTip = false

// install
Vue.use(VuejsClipper)
// install vue-rx
Vue.use(VueRx)


if (document.querySelector('#my-strictly-unique-vue-multiple-image-clipping')) {
  Vue.component('VueMultipleImageClipping', VueMultipleImageClipping)

  new Vue({
    el: '#my-strictly-unique-vue-multiple-image-clipping',
    render: h => h(App)
  })
}

// export default VueMultipleImageClipping
